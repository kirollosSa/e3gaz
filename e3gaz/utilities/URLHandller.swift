//
//  URLHandller.swift
//  Kadi
//
//  Created by apple on 12/4/17.
//  Copyright © 2017 apple. All rights reserved.
//

import Foundation

class URLHandller {
    
    static let GOOGLE_API_KEY = "AIzaSyCxnCjCCOV6IZrsxaKvqP8CbyAIO2xhOcI"
    
    static let BASE_URL = "http://ejazq.com/web_service/go.php"
    static let FILE_BASE_URL = "http://ejazq.com/admin/app/webroot/files/"
    static let youtubeBaseUrl = "https://www.youtube.com/embed/"
   
    enum APPURL{
        case categories
        case posts
        case live
        case teams
        case subjects
    }
    
    static func headerUrl(base_url:APPURL) -> String {
        switch base_url {
        case .categories:
            return "\(FILE_BASE_URL)categories/"
        case .posts:
            return "\(FILE_BASE_URL)posts/"
        case .live:
            return "\(FILE_BASE_URL)"
        case .teams:
            return "\(FILE_BASE_URL)teams/"
        case .subjects:
            return "\(FILE_BASE_URL)subjects/"
        }
        
        
    }
    
}
