//
//  Constants.swift
//  Kadi
//
//  Created by apple on 12/4/17.
//  Copyright © 2017 apple. All rights reserved.
//

import Foundation

let SHARE_APP_MESSAGE: String = "حمل اتطبيق كادي  من المتجر مجانا : https://itunes.apple.com/us/developer/mohamed-mostafa/id1022020436 "
let PHONE_ERROR_MESSAGE: String =  "يجب إدخال رقم الهاتف بشكل صحيح"

let MAIL_ERROR_MESSAGE: String = "يجب إدخال البريد الإلكتوني بشكل صحيح"
let PASSWORD_ERROR_MESSAGE : String = "يجب تطابق كلمة المرور"
let LOCATION_MESSAGE: String = "يرجي تفعيل المواقع للوصول الي أقرب شاحنات"
let COMPLETE_DATA_ERROR: String = "يرجي استكمال البيانات بشكل صحيح"
let LOGIN_ERROR_MESSAGE: String = "يرجي تسجيل الدخول من القائمة الجانبية"


let APP_LOGIN = "AppLogin"
let DEVICE_TOKEN = "DeviceToken"
let DEVICE_ID = UIDevice.current.identifierForVendor!.uuidString
let TO_IMAGE = NSNotification.Name("TO_IMAGE")
let TO_VIDEO = NSNotification.Name("TO_VIDEO")
let TO_BOOK = NSNotification.Name("TO_BOOK")
let TO_ARTICLE = NSNotification.Name("TO_ARTICLE")
let APP_AWAKE: String? = ""




