//
//  extention.swift
//  e3gaz
//
//  Created by apple on 8/15/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

extension UIViewController{
    
    func pushBarButtonLeft(){
        self.tabBarController?.tabBar.isHidden = true
        let barButton = UIBarButtonItem(image: UIImage(named: "22"), landscapeImagePhone: nil, style: .done, target: self, action: #selector(BackClicked))
        self.navigationItem.leftBarButtonItem = barButton
    }
    @objc func BackClicked() {
        isFirstTime()
    }
    func isFirstTime(){
        if let y = UserDefaults.standard.object(forKey: "login_State") as? String
        {
            if  y == "AppLogin" {
                // open home page
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "MainTabBar") as UIViewController
                UIApplication.shared.keyWindow?.rootViewController = vc
            }
            else
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginNav") as UIViewController
                UIApplication.shared.keyWindow?.rootViewController = vc
            }
        }
        else
        {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginNav") as UIViewController
            UIApplication.shared.keyWindow?.rootViewController = vc
        }
    }
}
