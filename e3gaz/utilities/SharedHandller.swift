//
//  SharedHandller.swift
//  Tamwile
//
//  Created by xwady on 9/27/17.
//  Copyright © 2017 xwady. All rights reserved.
//

import Foundation

//import MaterialComponents
import  Alamofire
import  SwiftyJSON
import MapKit

class SharedHandller {

    /*
     {
     "method":"update_zenhom",
     "value":"7"
     }
     
 */
    
    ////////////////// Side Menu ///////////////////////////////
    static var sideButton : UIBarButtonItem!

    static func setSideImage()  {
        let image = UIImage(named: "icon")

        sideButton = UIBarButtonItem(image: image,  style: .plain , target: self, action: Selector(("didTapEditButton:")))
    }

    static func sideMenus(control: UIViewController)
    {
        setSideImage()
        if control.revealViewController() != nil
        {
            sideButton.target = control.revealViewController()
            sideButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            control.navigationItem.leftBarButtonItem = sideButton
            control.revealViewController().rearViewRevealWidth = UIScreen.main.bounds.width - 50.0
            control.view.addGestureRecognizer(control.revealViewController().panGestureRecognizer())
        }
    }
    
   

    
    /////////////////// cashed Data /////////////////////
    
    static func Value(of userDeafult : String) -> String {
        let y = UserDefaults.standard.object(forKey: userDeafult) as? String ?? "nil"
        return y
    }
    
    static func setValue(_ Value : String, forKey : String)  {
        UserDefaults.standard.set(Value, forKey: forKey)
        UserDefaults.standard.synchronize()
        
    }

    static func GetUserID() -> String
    {
        let y = UserDefaults.standard.object(forKey: "DEFuserid") as? String ?? ""
        return y
    }


    static func getDeviceToken() -> String {
        let y = UserDefaults.standard.object(forKey: "DeviceToken") as? String ?? ""
        return y
    }

    static func LoginState() -> String {
        let y = UserDefaults.standard.object(forKey: "login_State") as? String ?? ""
        return y
    }

    
    ////////// DATES FROM TIME STAMP
    
    static func changeDate(dates: String) -> String{
        
        let ddd = Double(dates)
        let date = NSDate(timeIntervalSince1970: ddd!)
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd-MM-YYYY hh:mm a"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
        
    }
    
    ////////////////// reusable Methods //////////////////////
    static func StatusIs(code: Int) -> String {
        if code == 101 {
            return "sucess"
        }
        if code == 102 {
            return "empty"
        }
        if code == 103 {
            return "wrong_mail"
        }
        if code == 104 {
            return "failed"
        }
        if code == 105 {
            return "used_mail"
        }
        if code == 106 {
            return "wrong_password"
        }
        if code == 107 {
            return "rent_before"
        }
        if code == 108 {
            return "deleted"
        }
        return ""
    }

    static func isValidAction(textFeilds: [UITextField]) -> Bool {
      
        for myTextFeild in textFeilds {
            if (myTextFeild.text!.isEmpty) {
                return false
            }
        }
        return true
    }
    
    
    /////// youtueb Safari Or Appfalse
    static func playInYoutube(youtubeId: String) {
        if let youtubeURL = URL(string: "youtube://\(youtubeId)"),
            UIApplication.shared.canOpenURL(youtubeURL) {
            // redirect to app
            UIApplication.shared.open(youtubeURL, options: [:], completionHandler: nil)
        } else if let youtubeURL = URL(string: "https://www.youtube.com/watch?v=\(youtubeId)") {
            // redirect through safari
            UIApplication.shared.open(youtubeURL, options: [:], completionHandler: nil)
        }
    }

    static func MessageHandler(viewContro: UIViewController,title: String,message: String,submitButton: String,myHandler: (() -> Swift.Void)? = nil)
    {
        let alert = UIAlertController(title: title, message: message , preferredStyle: UIAlertControllerStyle.alert)

        //title

        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: title , attributes: [NSAttributedStringKey.font:UIFont(name: "HacenTunisia", size: 18.0)!])
        myMutableString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:title.characters.count))
        alert.setValue(myMutableString, forKey: "attributedTitle")

        // message

        var messageMutableString = NSMutableAttributedString()
        messageMutableString = NSMutableAttributedString(string: message, attributes: [NSAttributedStringKey.font:UIFont(name: "HacenTunisia", size: 13.0)!])
        messageMutableString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: NSRange(location:0,length:message.characters.count))
        alert.setValue(messageMutableString, forKey: "attributedMessage")


        // action Button
        let action: UIAlertAction!
        if myHandler?() == nil {
            action = UIAlertAction(title: submitButton, style: UIAlertActionStyle.default, handler:nil)
        } else {
            action = UIAlertAction(title: submitButton, style: UIAlertActionStyle.default, handler: { _ in
                myHandler!()
            })
        }
        action.setValue(UIColor.orange, forKey: "titleTextColor")
        alert.addAction(action)
        alert.view.tintColor = UIColor.orange
        viewContro.present(alert, animated: true, completion: nil)



    }


    // to get All fonts

    static func LoopFonts() {
        UIFont.familyNames.forEach({ familyName in
            let fontNames = UIFont.fontNames(forFamilyName: familyName)
        })
    }

    static func generateRandomDigits(_ digitNumber: Int) -> String {
        var number = ""
        for i in 0..<digitNumber {
            var randomNumber = arc4random_uniform(10)
            while randomNumber == 0 && i == 0 {
                randomNumber = arc4random_uniform(10)
            }
            number += "\(randomNumber)"
        }
        return number
    }

///////////////// open setting method
    
    
    
}



