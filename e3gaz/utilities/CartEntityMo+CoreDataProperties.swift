//
//  CartEntityMo+CoreDataProperties.swift
//  
//
//  Created by apple on 12/25/17.
//
//

import Foundation
import CoreData


extension CartEntityMo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CartEntityMo> {
        return NSFetchRequest<CartEntityMo>(entityName: "CartEntity")
    }

        @NSManaged public var image: String?
        @NSManaged public var sheep_name: String?
        @NSManaged public var totalCoast: String?
        @NSManaged public var sheep_desc: String?
        @NSManaged public var sheep_details: String?
        @NSManaged public var id: String?
        @NSManaged public var size_price: String?
        @NSManaged public var kilo_price: String?
        @NSManaged public var service_coast: String?
        @NSManaged public var quantity: String?
        @NSManaged public var size_value: String?
        @NSManaged public var slice_id: String?
        @NSManaged public var preperatipn_id: String?
    
}
