//
//  ByGrandViewController.swift
//  5rof
//
//  Created by apple on 12/19/17.
//  Copyright © 2017 apple. All rights reserved.
//

import UIKit

class ByGrandViewController: UIViewController {

  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        showAnimate()
        
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func closeButton(_ sender: Any) {
        removeAnimation()
    }
    
    @IBAction func openSite(_ sender: Any) {
        if let url = URL(string: "http://2grand.net") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    @IBAction func call(_ sender: Any) {
        let url:NSURL = NSURL(string: "tel://00201157771069")!
        
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    }
    @IBAction func whatsApp(_ sender: Any) {
        let date = Date()
        let msg = "Hello \(date)"
        let urlWhats = "whatsapp://send?phone=+201157771069&abid=00201157771069&text=\(msg)"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = NSURL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                    UIApplication.shared.open(whatsappURL as URL, options: [:], completionHandler: nil)
                } else {
                    SharedHandller.MessageHandler(viewContro: self, title: "تنبيه", message: "يجب تنزيل واتساب أولا", submitButton: "تم")
                }
            }
        }
    }
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    func removeAnimation() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
            self.view.alpha = 0.0;
        }) { (finished : Bool) in
            if finished {
                self.view.removeFromSuperview()
            }
        }
    }



}
