//
//  GifVC.swift
//  duawin
//
//  Created by hesham ghalaab on 8/7/17.
//  Copyright © 2017 Grand. All rights reserved.
//

import UIKit
import Gifu
import SwiftyJSON
import Alamofire
import KMPopUp

//import NVActivityIndicatorView


class GifVC: UIViewController {
    
    
    @IBOutlet weak var gifImageViewX: GIFImageView!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        let y = UserDefaults.standard.object(forKey: "login_State") as? String
        print("10002",y ?? String())
        
        gifImageViewX.animate(withGIFNamed: "1--Splash2", loopCount: 1)
        
        // indecatorView.startAnimating()
        ifInReview()
        perform(#selector(checkIfFirstLaunch), with: nil, afterDelay: 3.0)
    }
    

    
    @objc func checkIfFirstLaunch() {
        isFirstTime()
    }
    
    func ifInReview() {
        let param = ["method": "get_setting"]
        Alamofire.request(URLHandller.BASE_URL,  method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
            .responseJSON { res in
                switch res.result {
                    
                case .success(let val):
                    let json = JSON(val)
                    let zenhom = json[0]["zenhom"].string!
                    if zenhom == "1" {
                        UserDefaults.standard.set("true", forKey: "inTestinPhase")
                    } else {
                        UserDefaults.standard.set("false", forKey: "inTestinPhase")
                    }
                    UserDefaults.standard.synchronize()
                   
                case .failure(let err):
                    SharedHandller.MessageHandler(viewContro: self, title: "خطأ", message: err.localizedDescription, submitButton: "تم")
                }
        }
    }
    
    
}


