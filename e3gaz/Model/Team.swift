//
//  Team.swift
//  e3gaz
//
//  Created by apple on 2/22/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation


class Team {

    var id :String!
    var name :String!
    var details :String!
    var date :String!
    var image :String!
    
    init(id :String,name :String,details :String,date :String,image :String) {
        self.id = id
        self.name = name
        self.details = details
        self.date = date
        self.image = image
    }
}
