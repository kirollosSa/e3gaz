//
//  News.swift
//  e3gaz
//
//  Created by apple on 2/21/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation

class News {
    var id : String!
    var name : String!
    var details : String!
    var image : String!
    var created: String!
    var subject_id : String!
    var youtube : String!
    
    init(id : String,name : String,details : String,image : String,created: String,subject_id : String,youtube: String) {
        self.id = id
        self.name = name
        self.details = details
        self.image = image
        self.created = created
        self.subject_id = subject_id
        self.youtube = youtube
    }
}


