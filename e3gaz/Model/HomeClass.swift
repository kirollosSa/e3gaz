//
//  HomeClass.swift
//  e3gaz
//
//  Created by apple on 2/15/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation

class HomeClass {
   
    var id : String!
    var name: String!
    var image: String!
    var elments: [ElmentsClass]!
    var created: String!
    
    init(id : String,name : String,elments: [ElmentsClass],image : String,created: String) {
        self.id = id
        self.name = name
        self.elments = elments
        self.image = image
        self.created = created
    }
    
}

class ElmentsClass {

    var id : String!
    var name : String!
    var details : String!
    var image : String!
    var created: String!
    var subject_id : String!
    
    init(id : String,name : String,details : String,image : String,created: String,subject_id : String) {
        self.id = id
        self.name = name
        self.details = details
        self.image = image
        self.created = created
        self.subject_id = subject_id
    }
    
}
