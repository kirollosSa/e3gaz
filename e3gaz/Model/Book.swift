//
//  Book.swift
//  e3gaz
//
//  Created by apple on 4/4/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation

class Book {
    let id : String?
    let name : String?
    let details : String?
    let image : String?
    let pdf : String?
    let size : String?
    
    init(id : String,name : String,details : String,image : String,pdf : String,size : String) {
        self.id = id
        self.name = name
        self.details = details
        self.image = image
        self.pdf = pdf
        self.size = size
    }

}
