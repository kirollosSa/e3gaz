//
//  MuchViewsViewController.swift
//  e3gaz
//
//  Created by apple on 2/19/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import KMPopUp

class MuchViewsViewController: UIViewController , UITableViewDataSource , UITableViewDelegate {

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyBox: UIImageView!
    
    var List = [ElmentsClass]()
    override func viewDidLoad() {
        super.viewDidLoad()

        loadImages()
        // Do any additional setup after loading the view.
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return List.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ImageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ImageTableViewCell
        
        cell.selectionStyle = .none
        cell.setImage(url: "\(URLHandller.headerUrl(base_url: URLHandller.APPURL.posts))\(List[indexPath.row].image!)")
        cell.lableText.text = List[indexPath.row].name!
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SingleImageViewController") as! SingleImageViewController
        vc.elementsDetails = List[indexPath.row]
        
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func loadImages() {
        let param = ["method":"images"]
        self.indicator.startAnimating()
        Alamofire.request(URLHandller.BASE_URL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
            .responseJSON { res in
                switch res.result {
                    
                case .success(let val):
                    let json = JSON(val)
                    let more_scenes = json["more_scenes"].array!
                    if more_scenes.count > 0 {
                    for index in 0..<more_scenes.count {
                        
                        var id : String = "nil"
                        var name: String = "nil"
                        var image: String = "nil"
                        var subject_id : String = "nill"
                        var details: String = "nil"
                        
                        if let pid = more_scenes[index]["id"].string { id = pid }
                        if let pname = more_scenes[index]["name"].string { name = pname }
                        if let pimage = more_scenes[index]["image"].string { image = pimage }
                        if let pdetails = more_scenes[index]["details"].string { details = pdetails }
                        if let psubject_id = more_scenes[index]["subject_id"].string { subject_id = psubject_id }
                        
                        
                        self.List.append(ElmentsClass(id: id, name: name, details: details, image: image, created: "", subject_id: subject_id))
                        
                    }
                    self.indicator.stopAnimating()
                        self.tableView.isHidden = false
                    self.tableView.reloadData()
                    } else {
                        self.emptyBox.isHidden = false
                    }
                case .failure(let err):
                    self.indicator.stopAnimating()
                    KMPoUp.ShowMessage(controller: self, message: err.localizedDescription, image: #imageLiteral(resourceName: "warning"))
                }
        }
    }


}
