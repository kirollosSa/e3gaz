//
//  ImageTableViewCell.swift
//  e3gaz
//
//  Created by apple on 2/19/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class ImageTableViewCell: UITableViewCell {

    @IBOutlet weak var subImage: UIImageView!
    @IBOutlet weak var lableText: UILabel!
    @IBOutlet weak var webView: UIWebView!
    
    @IBOutlet weak var lableDate: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setImage(url : String) {
        DispatchQueue.main.async {
            self.subImage.sd_setImage(with: URL(string: url))
        }
    }
    
    func setVideo(videoCode: String) {
        webView.loadRequest(URLRequest(url: URL(string: "\(URLHandller.youtubeBaseUrl)\(videoCode)")!))
    }

}
