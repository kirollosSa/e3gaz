//
//  ImageCollectionViewCell.swift
//  e3gaz
//
//  Created by apple on 2/19/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var fileSize: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var lableText: UILabel!
    
    func setImage(url : String) {
        DispatchQueue.main.async {
            self.image.sd_setImage(with: URL(string: url))
        }
    }
    
}
