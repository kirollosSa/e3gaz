//
//  SingleImageViewController.swift
//  e3gaz
//
//  Created by apple on 2/19/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SingleImageViewController: UIViewController, UIScrollViewDelegate {
    
  //  var postID = " "
    var isNotification = false
    
    @IBOutlet weak var scrolling: UIScrollView!
    @IBOutlet weak var subImage: UIImageView!
    @IBOutlet weak var subDetails: UILabel!
    @IBOutlet weak var followButton: UIButton!
    
    var elementsDetails : ElmentsClass!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isNotification{
            getPost()
   //         pushBarButtonLeft()
//            self.navigationController?.navigationBar.tintColor = UIColor.white
//            self.navigationController?.navigationBar.barTintColor = UIColor(displayP3Red: 74, green: 80, blue: 91, alpha: 1)
        }else{
            sendView()
            
            navigationItem.title = elementsDetails.name!
            subDetails.text = elementsDetails.details!
            setImage(url: "\(URLHandller.headerUrl(base_url: URLHandller.APPURL.posts))\(elementsDetails.image!)")
        }
        
        

        scrolling.delegate = self
        
        scrolling.minimumZoomScale = 1.0
        scrolling.maximumZoomScale = 10.0//maximum zoom scale you want
        scrolling.zoomScale = 1.0
        // Do any additional setup after loading the view.
    }

    func setImage(url : String) {
        DispatchQueue.main.async {
            self.subImage.sd_setImage(with: URL(string: url))
        }
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return subImage
    }
    func sendView() {
        let param = ["method":"update_post",
                     "post_id":"\(elementsDetails.id!)"]
        Alamofire.request(URLHandller.BASE_URL,  method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
            .responseJSON { res in
                switch res.result {
                    
                case .success(let val):
                    let json = JSON(val)
                case .failure( let err):
                    print(err.localizedDescription)
                }
        }
    }
    func getPost() {
        let param = ["method":"get_post_id",
                     "post_id":"\(SharedHandller.Value(of: "TARGET_ID"))"]
        Alamofire.request(URLHandller.BASE_URL,  method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
            .responseJSON { res in
                switch res.result {
                    
                case .success:
                    if let Data = res.result.value as? NSArray{
                        for data in Data{
                            
                            if let object = data as? Dictionary <String, Any>{
                                if let name = object["name"] as? String {
                                    self.navigationItem.title = name
                                }
                                if let det = object["details"] as? String{
                                    self.subDetails.text = det
                                }
                                if let image = object["image"] as? String{
                                    self.setImage(url: "\(URLHandller.headerUrl(base_url: URLHandller.APPURL.posts))\(image)")
                                }
                            }
                        }
                    }
                case .failure( let err):
                    print(err.localizedDescription)
                }
        }
    }
}
