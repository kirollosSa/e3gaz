//
//  ImagesViewController.swift
//  e3gaz
//
//  Created by apple on 2/18/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class ImagesViewController: UIViewController {

    @IBOutlet weak var muchViews: UIView!
    @IBOutlet weak var newImages: UIView!
    
    @IBOutlet weak var segment: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        segment.selectedSegmentIndex = 1

        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func searchButton(_ sender: Any) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func sideButton(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SidePageViewController") as! SidePageViewController
        
        navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func segment(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            UIView.animate(withDuration: 0.5, animations: {
                self.newImages.alpha = 0.0
                self.muchViews.alpha = 1.0
            })
        } else if sender.selectedSegmentIndex == 1 {
            UIView.animate(withDuration: 0.5, animations: {
                self.newImages.alpha = 1.0
                self.muchViews.alpha = 0.0
            })
        }
    }
    
}
