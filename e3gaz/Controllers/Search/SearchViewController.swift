//
//  SearchViewController.swift
//  e3gaz
//
//  Created by apple on 2/25/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import KMPopUp


class SearchViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {

    @IBOutlet weak var emptyBox: UIImageView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchText: UITextField!
    var List = [ElmentsClass]()
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "بحث"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func searchButton(_ sender: Any) {
        if SharedHandller.isValidAction(textFeilds: [searchText]) {
            self.view.endEditing(true)
            let param = ["method":"search",
                         "title":"\(searchText.text!)"]
            searchOnline(param)
        } else {
            KMPoUp.ShowMessageWithDuration(controller: self, message: "ادخل كلمة البحث", image: #imageLiteral(resourceName: "warning"), duration: 1.5)
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return List.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ImageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ImageTableViewCell", for: indexPath) as! ImageTableViewCell
        cell.setImage(url: "\(URLHandller.headerUrl(base_url: URLHandller.APPURL.posts))\(List[indexPath.row].image!)")
        cell.lableText?.text = List[indexPath.row].details!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 145
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SingleSubjectViewController") as! SingleSubjectViewController
        vc.elementsDetails = List[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func searchOnline(_ param: Parameters) {
        
        self.indicator.startAnimating()
        self.List.removeAll()
        Alamofire.request(URLHandller.BASE_URL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
            .responseJSON { res in
                switch res.result {
                    
                case .success(let val):
                    let new = JSON(val)
                   
                    if new.count > 0 {
                        for index in 0..<new.count {
                            
                            var id : String = "nil"
                            var name: String = "nil"
                            var image: String = "nil"
                            var subject_id : String = "nill"
                            var details: String = "nil"
                            var created: String = "nil"
                            
                            
                            if let pid = new[index]["id"].string { id = pid }
                            if let pname = new[index]["name"].string { name = pname }
                            if let pimage = new[index]["image"].string { image = pimage }
                            if let pdetails = new[index]["details"].string { details = pdetails }
                             if let pcreated = new[index]["created"].string { created = pcreated }
                            if let psubject_id = new[index]["subject_id"].string { subject_id = psubject_id }
                            
                            
                            self.List.append(ElmentsClass(id: id, name: name, details: details, image: image, created: created, subject_id: subject_id))
                            
                        }
                        
                        self.indicator.stopAnimating()
                        self.emptyBox.isHidden = true
                        self.tableView.isHidden = false
                        self.tableView.reloadData()
                    } else {
                        self.indicator.stopAnimating()
                        self.emptyBox.isHidden = false
                    }
                    
                case .failure(let err):
                    self.indicator.stopAnimating()
                    KMPoUp.ShowMessage(controller: self, message: err.localizedDescription, image: #imageLiteral(resourceName: "warning"))
                }
        }
    }

}
