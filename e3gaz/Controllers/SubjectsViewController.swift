//
//  SubjectsViewController.swift
//  e3gaz
//
//  Created by apple on 2/18/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class SubjectsViewController: UIViewController , UITableViewDataSource , UITableViewDelegate {
    
    @IBOutlet weak var emptyBox: UIImageView!
    var header: String = ""
    @IBOutlet weak var tableView: UITableView!
    var List = [ElmentsClass]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if List.count > 0 {
            tableView.isHidden = false
        } else {
            tableView.isHidden = true
            emptyBox.isHidden = false
        }

        navigationItem.title = header
        // Do any additional setup after loading the view.
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return List.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : SubjectsTableViewCell!
        
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! SubjectsTableViewCell
            cell.setImage(url:"\( URLHandller.headerUrl(base_url: URLHandller.APPURL.posts))\(List[indexPath.row].image!)")
            cell.subDate.text = SharedHandller.changeDate(dates: List[indexPath.row].created!)
            cell.subTitle.text = List[indexPath.row].name!
            cell.selectionStyle = .none
            return cell
        }
        else {
            cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! SubjectsTableViewCell
            cell.setImage(url:"\( URLHandller.headerUrl(base_url: URLHandller.APPURL.posts ))\(List[indexPath.row].image!)")
            cell.subDate.text = SharedHandller.changeDate(dates: List[indexPath.row].created!)
            cell.subTitle.text = List[indexPath.row].name!
            cell.subDetails.text = List[indexPath.row].details!
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SingleSubjectViewController") as! SingleSubjectViewController
        vc.elementsDetails = List[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
        
    }

}
