//
//  VideoSinglePageViewController.swift
//  e3gaz
//
//  Created by apple on 10/2/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import AVKit
import Alamofire
import SwiftyJSON
import KMPopUp

class VideoSinglePageViewController: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var MP4View: UIView!
    @IBOutlet weak var youTubeView: UIView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    @IBOutlet weak var VidName: UILabel!
    @IBOutlet weak var VidDate: UILabel!
    @IBOutlet weak var YoutubeDate: UILabel!
    @IBOutlet weak var YoutubeNAme: UILabel!
    @IBOutlet weak var VideoButton: UIButton!
    
    
    //MARK: View Properties
    var VideoModel: News?
    var isNotification = false
    var VideoURL = ""
    
    //MARK- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isNotification{
            getPost()
           
        }else{
            navigationItem.title = VideoModel!.name!
            setupView()
        }
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Helper Methods
    func setupView() {
        if VideoModel!.youtube.suffix(4) == ".mp4" {
            // MP4
            self.youTubeView.alpha = 0
            self.MP4View.alpha = 1
            self.VidDate.text = SharedHandller.changeDate(dates: VideoModel!.created!)
            self.VidName.text = VideoModel!.name!
            self.VideoButton.sd_setBackgroundImage(with: URL(string: URLHandller.headerUrl(base_url: .posts) + VideoModel!.image!), for: UIControlState.normal, completed: nil)
        }
        else {
            // Youtube
            setVideo(videoCode: VideoModel!.youtube!)
            YoutubeDate.text = SharedHandller.changeDate(dates: VideoModel!.created!)
            YoutubeNAme.text = VideoModel!.name!
            self.MP4View.alpha = 0
            self.youTubeView.alpha = 1
        }
    }
    
    func setVideo(videoCode: String) {
        webView.loadRequest(URLRequest(url: URL(string: "\(URLHandller.youtubeBaseUrl)\(videoCode)")!))
    }
    
    
    //MARK:- Actions
    @IBAction func PlayMP4Video(_ sender: Any) {
        var videoURL : URL!
        if isNotification {
             videoURL = URL(string: "\(URLHandller.headerUrl(base_url: .posts))\(VideoURL)")
        } else {
             videoURL = URL(string: "\(URLHandller.headerUrl(base_url: .posts))\(VideoModel!.youtube!)")
        }
        
            let player = AVPlayer(url: videoURL)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
    }
    
    //MARK:- Request Online
    func getPost() {
        indicator.startAnimating()
        let param = ["method":"get_post_id",
                     "post_id":"\(SharedHandller.Value(of: "TARGET_ID"))"]
        print(param)
        print(URLHandller.BASE_URL)
        Alamofire.request(URLHandller.BASE_URL,  method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
            .responseJSON { res in
                switch res.result {
                    
                case .success:
                    print(res.value!)
                    if let Data = res.result.value as? NSArray{
                        for data in Data{
                            
                            if let object = data as? Dictionary <String, Any>{
                                if self.isNotification {
                                    guard let created = object["created"] as? String else {return}
                                    guard let name = object["name"] as? String else {return}
                                    guard let image = object["image"] as? String else {return}
                                    guard let youtube = object["youtube"] as? String else { return }
                                    if youtube.suffix(4) == ".mp4" {
                                        // MP4
                                        
                                        self.youTubeView.alpha = 0
                                        self.MP4View.alpha = 1
                                        self.VidDate.text = SharedHandller.changeDate(dates: created)
                                        self.VidName.text = name
                                        self.VideoURL = youtube
                                        self.VideoButton.sd_setBackgroundImage(with: URL(string: URLHandller.headerUrl(base_url: .posts) + image), for: UIControlState.normal, completed: nil)
                                    } else {
                                        // Youtube
                                        self.setVideo(videoCode: youtube)
                                        self.YoutubeDate.text = SharedHandller.changeDate(dates: created)
                                        self.YoutubeNAme.text = name
                                        self.MP4View.alpha = 0
                                        self.youTubeView.alpha = 1
                                    }
                                }
                                self.indicator.stopAnimating()
                            }
                        }
                    }
                case .failure( let err):
                    print(err.localizedDescription)
                }
        }
    }
    
}
