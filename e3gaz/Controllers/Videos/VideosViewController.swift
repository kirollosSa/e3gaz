//
//  VideosViewController.swift
//  e3gaz
//
//  Created by apple on 2/21/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import KMPopUp
import Alamofire
import SwiftyJSON
import AVKit

class VideosViewController: UIViewController , UITableViewDataSource , UITableViewDelegate {

    
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var emptyBox: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var List = [News]()
    var MostList = [News]()
    var selectedList = [News]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(forName: TO_VIDEO, object: nil, queue: nil) { notification in
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VideoSinglePageViewController") as! VideoSinglePageViewController
            vc.isNotification = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        segment.selectedSegmentIndex = 1
        LoadVideos()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func searchButton(_ sender: Any) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func sideButton(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SidePageViewController") as! SidePageViewController
        
        navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func segment(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            if MostList.count > 0 {
                selectedList = MostList
                tableView.isHidden = false
                emptyBox.isHidden = true
                tableView.reloadData()
            } else {
                tableView.isHidden = true
                emptyBox.isHidden = false
            }
        } else {
            if List.count > 0 {
                selectedList = List
                tableView.isHidden = false
                emptyBox.isHidden = true
                tableView.reloadData()
            } else {
                tableView.isHidden = true
                emptyBox.isHidden = false
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if selectedList[indexPath.row].youtube.suffix(4) == ".mp4" {
            let cell : ImageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ImageTableViewCell
            
            cell.setImage(url: "\(URLHandller.headerUrl(base_url: .posts))\(selectedList[indexPath.row].image!)")
            cell.lableDate.text = SharedHandller.changeDate(dates: selectedList[indexPath.row].created!)
            cell.lableText.text = selectedList[indexPath.row].name!
            return cell
        } else {
            let cell : ImageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "youtube", for: indexPath) as! ImageTableViewCell
            
            cell.setVideo(videoCode: selectedList[indexPath.row].youtube!)
            cell.lableDate.text = SharedHandller.changeDate(dates: selectedList[indexPath.row].created!)
            cell.lableText.text = selectedList[indexPath.row].name!
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if selectedList[indexPath.row].youtube.suffix(4) == ".mp4" {
//            let videoURL = URL(string: "\(URLHandller.headerUrl(base_url: .posts))\(selectedList[indexPath.row].youtube!)")
//            
//            let player = AVPlayer(url: videoURL!)
//            let playerViewController = AVPlayerViewController()
//            playerViewController.player = player
//            self.present(playerViewController, animated: true) {
//                playerViewController.player!.play()
//            }
//        }
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VideoSinglePageViewController") as! VideoSinglePageViewController
        vc.VideoModel = selectedList[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func LoadVideos() {
        let param = ["method":"videos"]
        self.indicator.startAnimating()
        Alamofire.request(URLHandller.BASE_URL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
            .responseJSON { res in
                switch res.result {
                    
                case .success(let val):
                    let json = JSON(val)
                    let new = json["new"].array!
                    if new.count > 0 {
                        for index in 0..<new.count {
                            
                            var id : String = "nil"
                            var name: String = "nil"
                            var image: String = "nil"
                            var subject_id : String = "nill"
                            var details: String = "nil"
                            var youtube: String = "nil"
                            var created: String = "nil"
                            
                            if let pid = new[index]["id"].string { id = pid }
                            if let pname = new[index]["name"].string { name = pname }
                            if let pimage = new[index]["image"].string { image = pimage }
                            if let pdetails = new[index]["details"].string { details = pdetails }
                            if let psubject_id = new[index]["subject_id"].string { subject_id = psubject_id }
                            if let pyoutube = new[index]["youtube"].string { youtube = pyoutube }
                            if let pcreated = new[index]["created"].string { created = pcreated }
                            
                           
                                self.List.append(News(id: id, name: name, details: details, image: image, created: created, subject_id: subject_id, youtube: youtube))
                            
                        }
                        
                        self.indicator.stopAnimating()
                        self.selectedList = self.List
                        self.tableView.isHidden = false
                        self.tableView.reloadData()
                    } else {
                        self.emptyBox.isHidden = false
                    }
                    
                    let more_scenes = json["more_scenes"].array!
                        for index in 0..<new.count {
                            
                            var id : String = "nil"
                            var name: String = "nil"
                            var image: String = "nil"
                            var subject_id : String = "nill"
                            var details: String = "nil"
                            var youtube: String = "nil"
                            var created: String = "nil"
                            
                            if let pid = more_scenes[index]["id"].string { id = pid }
                            if let pname = more_scenes[index]["name"].string { name = pname }
                            if let pimage = more_scenes[index]["image"].string { image = pimage }
                            if let pdetails = more_scenes[index]["details"].string { details = pdetails }
                            if let psubject_id = more_scenes[index]["subject_id"].string { subject_id = psubject_id }
                            if let pyoutube = more_scenes[index]["youtube"].string { youtube = pyoutube }
                            if let pcreated = more_scenes[index]["created"].string { created = pcreated }
                            
                            
                            self.MostList.append(News(id: id, name: name, details: details, image: image, created: created, subject_id: subject_id, youtube: youtube))
                            
                        }
                case .failure(let err):
                    self.indicator.stopAnimating()
                    KMPoUp.ShowMessage(controller: self, message: err.localizedDescription, image: #imageLiteral(resourceName: "warning"))
                }
        }
    }
}
