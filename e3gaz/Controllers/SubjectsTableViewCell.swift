//
//  SubjectsTableViewCell.swift
//  e3gaz
//
//  Created by apple on 2/18/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class SubjectsTableViewCell: UITableViewCell, UIScrollViewDelegate{
    
    @IBOutlet weak var SubImage: UIImageView!
    @IBOutlet weak var subDate: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var subDetails: UILabel!
    @IBOutlet weak var scrolling: UIScrollView!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        scrolling.delegate = self
        
        scrolling.minimumZoomScale = 1.0
        scrolling.maximumZoomScale = 10.0//maximum zoom scale you want
        scrolling.zoomScale = 1.0
    }

    func setImage(url : String) {
        DispatchQueue.main.async {
            self.SubImage.sd_setImage(with: URL(string: url))
        }
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return SubImage
    }
}
