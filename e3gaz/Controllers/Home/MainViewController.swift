//
//  HomeViewController.swift
//  e3gaz
//
//  Created by apple on 2/14/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import MediaPlayer
import AVKit


class MainViewController: UIViewController {
    
        override func viewDidLoad() {
            super.viewDidLoad()
            
        }
    
    @IBAction func dsd(_ sender: Any) {
        
        let videoURL = URL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4 ")
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
       
    }
    
}

