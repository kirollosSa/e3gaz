//
//  HomeCollectionViewCell.swift
//  e3gaz
//
//  Created by apple on 2/15/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import SDWebImage

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var textLabel: UILabel!
    
    func setImage(url : String) {
        DispatchQueue.main.async {
            self.image.sd_setImage(with: URL(string: url))
        }
    }
    
}
