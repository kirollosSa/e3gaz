//
//  HomeViewController.swift
//  e3gaz
//
//  Created by apple on 2/14/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import MediaPlayer
import Alamofire
import SwiftyJSON
import KMPopUp


class HomeViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {


    
    @IBOutlet weak var emptyBox: UIImageView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!
    var List = [HomeClass]()
    var fulSize:CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(forName: TO_IMAGE, object: nil, queue: nil) { notification in
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SingleImageViewController") as! SingleImageViewController
            vc.isNotification = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
        NotificationCenter.default.addObserver(forName: TO_BOOK, object: nil, queue: nil) { notification in
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BooksViewController") as! BooksViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
        NotificationCenter.default.addObserver(forName: TO_ARTICLE, object: nil, queue: nil) { notification in
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "singelNewViewController") as! singelNewViewController
            vc.isNotification = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
        let he:CGRect = UIScreen.main.bounds
        let v = (he.width / 2) - 22
        fulSize = CGFloat(v)
        
        let param = ["method":"home"]
        readData(param)
      
        // Do any additional setup after loading the view.
    }
    
    @IBAction func searchButton(_ sender: Any) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func sideButton(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SidePageViewController") as! SidePageViewController
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return List.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : HomeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! HomeCollectionViewCell
        
        cell.setImage(url: "\(URLHandller.headerUrl(base_url: URLHandller.APPURL.categories))\(List[indexPath.row].image!)")
        cell.textLabel.text = List[indexPath.row].name!
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: fulSize, height: fulSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SubjectsViewController") as! SubjectsViewController
        vc.List = List[indexPath.row].elments!
        vc.header = List[indexPath.row].name!
        navigationController?.pushViewController(vc, animated: true)
    
    }
    
    //////// req online
    func readData(_ param : Parameters ) {
        self.indicator.startAnimating()
        Alamofire.request(URLHandller.BASE_URL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
            .responseJSON { res in
                switch res.result {
                    
                case .success(let val):
                    let json = JSON(val)
                    if json.count > 0 {
                        for index in 0..<json.count {
                            
                            var id : String = "nil"
                            var name: String = "nil"
                            var image: String = "nil"
                            var elments: [ElmentsClass] = []
                            var created: String = "nil"
                            
                            if let pid = json[index]["id"].string { id = pid }
                            if let pname = json[index]["name"].string { name = pname }
                            if let pimage = json[index]["image"].string { image = pimage }
                            if let pcreated = json[index]["created"].string { created = pcreated }
                            if let pelments = json[index]["elments"].array {
                                for ind in 0..<pelments.count {
                                    elments.append(ElmentsClass(id: pelments[ind]["id"].string!, name: pelments[ind]["name"].string!, details: pelments[ind]["details"].string!, image: pelments[ind]["image"].string!, created: pelments[ind]["created"].string!, subject_id: pelments[ind]["subject_id"].string!))
                                }
                            }
                            
                            self.List.append(HomeClass(id: id, name: name, elments: elments, image: image, created: created))
                            
                        }
                        self.indicator.stopAnimating()
                        self.collectionView.isHidden = false
                        self.collectionView.reloadData()
                    } else {
                        self.emptyBox.isHidden = false
                    }
                   
                    
                case .failure(let err):
                    self.indicator.stopAnimating()
                    KMPoUp.ShowMessage(controller: self, message: err.localizedDescription, image: #imageLiteral(resourceName: "warning"))
                }
        }
    }
    
}
