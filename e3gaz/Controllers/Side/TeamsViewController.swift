//
//  TeamsViewController.swift
//  e3gaz
//
//  Created by apple on 2/22/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import KMPopUp

class TeamsViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet weak var emptyBox: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var List = [Team]()
    var fulSize: CGFloat = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "فريق الباحثين"
        let he:CGRect = UIScreen.main.bounds
        let v = (he.width / 2) - 22
        fulSize = CGFloat(v)
        loadTeam()
        // Do any additional setup after loading the view.
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return List.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : ImageCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImageCollectionViewCell
        
        cell.setImage(url: "\(URLHandller.headerUrl(base_url: .teams))\(List[indexPath.row].image!)")
        cell.lableText.text = List[indexPath.row].name!
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: fulSize, height: (fulSize
         * 1.25))
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc : SinglePersonViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SinglePersonViewController") as! SinglePersonViewController
        
        vc.person = List[indexPath.row]
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func loadTeam() {
        let param = ["method":"teams"]
        self.indicator.startAnimating()
        Alamofire.request(URLHandller.BASE_URL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
            .responseJSON { res in
                switch res.result {
                    
                case .success(let val):
                    let more_scenes = JSON(val)
                    if more_scenes.count > 0 {
                        for index in 0..<more_scenes.count {
                            
                            var id : String = "nil"
                            var name: String = "nil"
                            var image: String = "nil"
                            var date : String = "nill"
                            var details: String = "nil"
                            
                            if let pid = more_scenes[index]["id"].string { id = pid }
                            if let pname = more_scenes[index]["name"].string { name = pname }
                            if let pimage = more_scenes[index]["image"].string { image = pimage }
                            if let pdetails = more_scenes[index]["details"].string { details = pdetails }
                            if let pdate = more_scenes[index]["date"].string { date = pdate }
                            
                            
                            self.List.append(Team(id: id, name: name, details: details, date: date, image: image))
                            
                        }
                        self.indicator.stopAnimating()
                        self.collectionView.isHidden = false
                        self.collectionView.reloadData()
                    } else {
                        self.emptyBox.isHidden = false
                    }
                case .failure(let err):
                    self.indicator.stopAnimating()
                    KMPoUp.ShowMessage(controller: self, message: err.localizedDescription, image: #imageLiteral(resourceName: "warning"))
                }
        }
    }

}
