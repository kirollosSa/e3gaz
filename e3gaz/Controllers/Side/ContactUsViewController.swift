//
//  ContactUsViewController.swift
//  e3gaz
//
//  Created by apple on 2/25/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import KMPopUp
import Alamofire
import SwiftyJSON

class ContactUsViewController: UIViewController {

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var whatsApp: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var Message: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "اتصل بنا"

        // Do any additional setup after loading the view.
    }
    
    @IBAction func sendMEssage(_ sender: Any) {
        if SharedHandller.isValidAction(textFeilds: [phoneNumber,whatsApp,email]) {
            if !(Message.text.isEmpty) {
                if phoneNumber.text!.isPhone() && whatsApp.text!.isPhone() {
                    if UITextField.isValidEmail(testStr: email) {
                        let param = [    "method":"send_mesage",
                                         "whatsapp":"\(whatsApp.text!)",
                                         "message":"\(Message.text!)",
                                         "phone":"\(phoneNumber.text!)",
                                         "email":"\(email.text!)"]
                        getsettings(param)
                    } else {
                         KMPoUp.ShowMessageWithDuration(controller: self, message: MAIL_ERROR_MESSAGE, image: #imageLiteral(resourceName: "warning"), duration: 1.5)
                    }
                } else {
                    KMPoUp.ShowMessageWithDuration(controller: self, message: PHONE_ERROR_MESSAGE, image: #imageLiteral(resourceName: "warning"), duration: 1.5)
                }
            } else {
                 KMPoUp.ShowMessageWithDuration(controller: self, message: COMPLETE_DATA_ERROR, image: #imageLiteral(resourceName: "warning"), duration: 1.5)
            }
        } else {
            KMPoUp.ShowMessageWithDuration(controller: self, message: COMPLETE_DATA_ERROR, image: #imageLiteral(resourceName: "warning"), duration: 1.5)
        }
    }
    
    func getsettings(_ param: Parameters) {
        self.indicator.startAnimating()
        Alamofire.request(URLHandller.BASE_URL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
            .responseJSON { res in
                switch res.result {
                    
                case .success( _):
                   
                    self.indicator.stopAnimating()
                    self.email.text = ""
                    self.phoneNumber.text = ""
                    self.whatsApp.text = ""
                    self.Message.text = ""
                    
                    KMPoUp.ShowMessage(controller: self, image: #imageLiteral(resourceName: "like (3)"))
                    
                case .failure(let err):
                    self.indicator.stopAnimating()
                    KMPoUp.ShowMessage(controller: self, message: err.localizedDescription, image: #imageLiteral(resourceName: "warning"))
                }
        }
    }
    
}
