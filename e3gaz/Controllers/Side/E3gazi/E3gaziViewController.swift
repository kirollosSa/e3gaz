//
//  E3gaziViewController.swift
//  e3gaz
//
//  Created by apple on 2/25/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class E3gaziViewController: UIViewController {
    @IBOutlet weak var segment: UISegmentedControl!
    
    @IBOutlet weak var savedView: UIView!
    @IBOutlet weak var follwedView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "إعجازي"
        segment.selectedSegmentIndex = 1
        self.savedView.alpha = 0.0
        // Do any additional setup after loading the view.
    }

    @IBAction func segmentView(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 1 {
            UIView.animate(withDuration: 0.5, animations: {
                  self.savedView.alpha = 0.0
                self.follwedView.alpha = 1.0
            })
        } else if sender.selectedSegmentIndex == 0 {
            UIView.animate(withDuration: 0.5, animations: {
                  self.savedView.alpha = 1.0
                self.follwedView.alpha = 0.0
            })
        }
    }

}
