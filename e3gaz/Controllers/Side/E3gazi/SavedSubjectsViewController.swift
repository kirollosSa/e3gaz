//
//  SavedSubjectsViewController.swift
//  e3gaz
//
//  Created by apple on 2/25/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import CoreData
import KMPopUp

class SavedSubjectsViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataView: UIView!
    var List = [E3gazEntityMo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    func deleteAllRecords() {
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "E3gazEntity")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try PresistanceServce.contecxt.execute(deleteRequest)
            PresistanceServce.saveContext()
        } catch {
            print ("There was an error")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let fetchRequest  : NSFetchRequest<E3gazEntityMo> = E3gazEntityMo.fetchRequest()

        do {
            let testID = try PresistanceServce.contecxt.fetch(fetchRequest)
            if testID.count > 0 {
                List = testID
                //ConvertToElemetList(from: testID)
                tableView.isHidden = false
                tableView.reloadData()
            } else {
                tableView.isHidden = true
               noDataView.isHidden = false
            }
        } catch {
            fatalError("Failed to fetch : \(error.localizedDescription)")
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return List.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let ImageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ImageTableViewCell", for: indexPath) as! ImageTableViewCell
        
        ImageTableViewCell.setImage(url: "\(URLHandller.headerUrl(base_url: .posts))\(List[indexPath.row].image!)")
        ImageTableViewCell.lableText.text = List[indexPath.row].details!
        ImageTableViewCell.selectionStyle = .none
        
        return ImageTableViewCell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let myDeleteButton: UITableViewRowAction = UITableViewRowAction(style: .normal, title: "حذف") { (action, index) -> Void in
            
            tableView.isEditing = false
            self.deleteCell(indexPath: indexPath)
            
        }
        myDeleteButton.backgroundColor = UIColor.red
        
        // return buttons
        return [myDeleteButton]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SingleSubjectViewController") as! SingleSubjectViewController
        vc.savedElements = List[indexPath.row]
        vc.from = "saved"
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 145
    }
    
    
    func deleteCell(indexPath: IndexPath) {
        let fetchRequest: NSFetchRequest<E3gazEntityMo> = E3gazEntityMo.fetchRequest()
        
        fetchRequest.predicate = NSPredicate.init(format: "id==\(List[indexPath.row].id!)")
        
        PresistanceServce.contecxt.delete(self.List[indexPath.row])
        
        do {
            try PresistanceServce.contecxt.save() // <- remember to put this :)
        } catch {
            // Do something... fatalerror
        }
        
        
        self.List.remove(at: indexPath.row)
        self.tableView.deleteRows(at: [indexPath], with: .middle)
    }
}
