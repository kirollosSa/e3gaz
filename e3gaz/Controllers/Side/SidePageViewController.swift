//
//  SidePageViewController.swift
//  e3gaz
//
//  Created by apple on 2/22/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class SidePageViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let barButton = UIBarButtonItem(image: UIImage(named: "22"), landscapeImagePhone: nil, style: .done, target: self, action: #selector(revealBackClicked))
        self.navigationItem.rightBarButtonItem = barButton
        self.navigationItem.title = "إعجاز"
        self.navigationItem.hidesBackButton = true

        // Do any additional setup after loading the view.
    }
    
    @objc func revealBackClicked() {
        _ = navigationController?.popViewController(animated: true)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell\(indexPath.row)", for: indexPath)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TeamsViewController")
            
            navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.row == 1 {
            
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BooksViewController")
            
            navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.row == 2 {
            
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HelpsViewController")
            
            navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.row == 3 {
            
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "E3gaziViewController")
            
            navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.row == 4 {
            
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContactUsViewController")
            
            navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.row == 5 {
            let popUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ByGrandViewController") as! ByGrandViewController
            self.addChildViewController(popUpVC)
            popUpVC.view.frame = self.view.frame
            self.view.addSubview(popUpVC.view)
            popUpVC.didMove(toParentViewController: self)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1 {
            return 144
        } else if indexPath.row == 5 {
            return 44
        } else {
            return 144
        }
        
    }
}
