//
//  HelpsViewController.swift
//  e3gaz
//
//  Created by apple on 2/25/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import KMPopUp

class HelpsViewController: UIViewController {

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var LAbleText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "مساهمتكم"
        getsettings()
        // Do any additional setup after loading the view.
    }

    
    func getsettings() {
        let param = ["method":"get_setting"]
        self.indicator.startAnimating()
        Alamofire.request(URLHandller.BASE_URL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
            .responseJSON { res in
                switch res.result {
                    
                case .success(let val):
                    let json = JSON(val)
                    let details = json[0]["details"].string!
                    self.LAbleText.text = details
                   self.LAbleText.isHidden = false
                    self.indicator.stopAnimating()
                    
                case .failure(let err):
                    self.indicator.stopAnimating()
                    KMPoUp.ShowMessage(controller: self, message: err.localizedDescription, image: #imageLiteral(resourceName: "warning"))
                }
        }
    }

}
