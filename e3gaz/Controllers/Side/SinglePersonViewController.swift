//
//  SinglePersonViewController.swift
//  e3gaz
//
//  Created by apple on 2/22/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import SwiftyJSON

class SinglePersonViewController: UIViewController {

    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var details: UILabel!
    @IBOutlet weak var personImage: RoundImage!
    
    var person: Team!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        name.text = person.name!
        date.text = person.date!
        details.text = person.details!
        setImage(url: "\(URLHandller.headerUrl(base_url: .teams))\(person.image!)")
        
        // Do any additional setup after loading the view.
    }
    
    func setImage(url : String) {
        DispatchQueue.main.async {
            self.personImage.sd_setImage(with: URL(string: url))
        }
    }
    
    func sendView() {
        let param = ["method":"update_post",
                     "post_id":"\(person.id!)"]
        Alamofire.request(URLHandller.BASE_URL,  method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
            .responseJSON { res in
                switch res.result {
                    
                case .success(let val):
                    let json = JSON(val)
                case .failure( let err):
                    print(err.localizedDescription)
                }
        }
    }

}
