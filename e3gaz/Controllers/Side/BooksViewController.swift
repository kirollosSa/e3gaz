//
//  BooksViewController.swift
//  e3gaz
//
//  Created by apple on 2/22/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import PDFReader
import Alamofire
import SwiftyJSON
import KMPopUp


class BooksViewController: UIViewController ,UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    var List = [Book]()
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var EmptyBox: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var fulSize:CGFloat = 0.0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let he:CGRect = UIScreen.main.bounds
        let v = (he.width / 2) - 22
        fulSize = CGFloat(v)
       getBooks()
        
//        if isNotification{
//            pushBarButtonLeft()
//        }
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        indicator.stopAnimating()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return List.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : ImageCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImageCollectionViewCell
        cell.setImage(url: "\(URLHandller.headerUrl(base_url: .posts))\(List[indexPath.row].image!)")
        cell.lableText.text = List[indexPath.row].name!
        cell.fileSize.text = List[indexPath.row].size!
        return cell
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.indicator.startAnimating()
        DispatchQueue.global().async {
            
            let remotePDFDocumentURLPath = self.List[indexPath.row].pdf!
            let remotePDFDocumentURL = URL(string: remotePDFDocumentURLPath)!
            let document = PDFDocument(url: remotePDFDocumentURL)!
            
            DispatchQueue.main.async {
                let readerController = PDFViewController.createNew(with: document)
                self.navigationController?.pushViewController(readerController, animated: true)
            }
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: fulSize, height: (1.5*fulSize))
    }
    
    
    func getBooks() {
        indicator.startAnimating()
        let param = ["method":"get_books"]
        Alamofire.request(URLHandller.BASE_URL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
            .responseJSON { res in
                switch res.result {
                case .success(let val):
                    let json = JSON(val)
                    if json.count > 0 {
                    for index in 0..<json.count {
                        var id : String = "nil"
                        var name : String = "nil"
                        var details : String = "nil"
                        var image : String = "nil"
                        var pdf : String = "nil"
                        var size : String = "nil"
                        
                        if let pid = json[index]["id"].string { id = pid }
                        if let pname = json[index]["name"].string { name = pname }
                        if let pdetails = json[index]["details"].string { details = pdetails }
                        if let pimage = json[index]["image"].string { image = pimage }
                        if let ppdf = json[index]["pdf"].string { pdf = ppdf }
                        if let psize = json[index]["size"].string { size = psize }
                        
                        self.List.append(Book(id: id, name: name, details: details, image: image, pdf: pdf, size: size))
                        
                    }
                   
                    self.indicator.stopAnimating()
                    self.collectionView.isHidden = false
                    self.collectionView.reloadData()
                        self.EmptyBox.isHidden = true
                    } else {
                        self.indicator.stopAnimating()
                        self.collectionView.isHidden = true
                        self.collectionView.reloadData()
                        self.EmptyBox.isHidden = false
                    }
                    
                case .failure(let err):
                    self.indicator.stopAnimating()
                    KMPoUp.ShowMessage(controller: self, message: err.localizedDescription, image: #imageLiteral(resourceName: "warning"))
                }
        }
    }

    
}
