//
//  LiveViewController.swift
//  e3gaz
//
//  Created by apple on 2/19/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import AVKit
import PlayListPlayer
import SwiftyJSON
import Alamofire
import KMPopUp
import Gifu

class LiveViewController: UIViewController {
    
    
    @IBOutlet weak var RadioGIF: GIFImageView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var movieRenderingView: MovieRenderingView!
    let player: PlayListPlayer = PlayListPlayer.shared
    let RadioPlayer: PlayListPlayer = PlayListPlayer.shared
    
    @IBOutlet weak var radioButton: UIButton!
    @IBOutlet weak var tvButton: UIButton!
    @IBOutlet weak var playerIcon: UIImageView!
    
    var radioArray : [String] = []
    var videoArray : [String] = []
    
    var VArray : [URL] = []
    var RArray : [URL] = [] 
    var Playing : Bool = true
    var RadioPlaying : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        RadioGIF.animate(withGIFNamed: "Radio Ios")
        RadioGIF.stopAnimatingGIF()
        self.movieRenderingView.addTapGesture(tapNumber: 1, target: self, action: #selector(PlayHandller))
        
        loadData()
    }
    
    
    @objc func PlayHandller() {
        if Playing {
            playerIcon.isHidden = false
            player.pause()
            Playing = false
        } else {
            playerIcon.isHidden = true
            player.play()
            Playing = true
        }
    }
    
    @objc func TabAction() {
        UIView.animate(withDuration: 0.5, animations: {
            self.movieRenderingView.isHidden = true
            self.player.pause()
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if player.isPlaying()
        {
            TabAction()
        }
        RadioGIF.stopAnimatingGIF()
    }
    
    @IBAction func TVButton(_ sender: UIButton) {
        if VArray.count > 0 {
            setupPlayer()
            playerIcon.isHidden = true
            Playing = true
            movieRenderingView.isHidden = false
            if RadioPlayer.isPlaying() {
                RadioPlayer.pause()
                RadioGIF.stopAnimatingGIF()
            }
            player.play()
        } else {
            KMPoUp.ShowMessage(controller: self, message: "غير متاح الآن", image: #imageLiteral(resourceName: "warning"))
        }
    }
    
    @IBAction func CloseButton(_ sender: Any) {
        TabAction()
    }
    func setupPlayer() {
        player.set(playList: VArray)
        
        movieRenderingView.set(player: player)
    }
    
    func setupRadioPlayer() {
        RadioPlayer.set(playList: RArray)
    }
    
    
    @IBAction func Radio(_ sender: UIButton) {
        if RArray.count > 0 {
            if RadioPlaying {
                RadioGIF.stopAnimatingGIF()
                RadioPlaying = false
                RadioPlayer.pause()
            } else {
                setupRadioPlayer()
                RadioGIF.startAnimatingGIF()
                RadioPlaying = true
                RadioPlayer.play()
            }
        } else {
            KMPoUp.ShowMessage(controller: self, message: "غير متاح الآن", image: #imageLiteral(resourceName: "warning"))
        }
    }
    
    
    
    @IBAction func searchButton(_ sender: Any) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func sideButton(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SidePageViewController") as! SidePageViewController
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func loadData() {
        let param = ["method":"get_setting"]
        self.indicator.startAnimating()
        Alamofire.request(URLHandller.BASE_URL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
            .responseJSON { res in
                switch res.result {
                    
                case .success(let val):
                    let json = JSON(val)
                    
                    let live_sound = json[0]["live_sound"].string!
                    let live_video = json[0]["live_video"].string!
                    if live_sound != "" {
                        self.radioArray = live_sound.components(separatedBy: ",")
                        self.setURL(for : self.radioArray)
                    }
                    if live_video != "" {
                        self.videoArray = live_video.components(separatedBy: ",")
                        self.setURL(for : self.videoArray)
                    }
                    
                    self.tvButton.isEnabled = true
                    self.radioButton.isEnabled = true
                    
                    self.indicator.stopAnimating()
                    
                case .failure(let err):
                    self.indicator.stopAnimating()
                    KMPoUp.ShowMessage(controller: self, message: err.localizedDescription, image: #imageLiteral(resourceName: "warning"))
                }
        }
    }
    
    func setURL(for array: [String]) {
        for index in 0..<array.count {
            if array == videoArray {
                VArray.append(URL(string: "\(URLHandller.headerUrl(base_url: .live))\(array[index])")!)
                
            } else if array == radioArray {
                RArray.append(URL(string: "\(URLHandller.headerUrl(base_url: .live))\(array[index])")!)
                
            }
        }
    }
}
