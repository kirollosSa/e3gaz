//
//  ForgetPasswordViewController.swift
//  e3gaz
//
//  Created by apple on 2/26/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import KMPopUp
import Alamofire
import SwiftyJSON

class ForgetPasswordViewController: UIViewController {
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    @IBAction func Cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendMail(_ sender: Any) {
        if UITextField.isValidEmail(testStr: email) {
            sendCode()
        } else {
            KMPoUp.ShowMessage(controller: self, message: MAIL_ERROR_MESSAGE, image: #imageLiteral(resourceName: "warning"))
        }
    }
    
    func sendCode() {
        let param = ["method":"send_code",
                     "email":"\(email.text!)"]
        indicator.startAnimating()
        Alamofire.request(URLHandller.BASE_URL,  method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
            .responseJSON { res in
                switch res.result {
                    
                case .success(let val):
                    let json = JSON(val)
                    
                    let result = json["result"].string!
                    if result == "send message" {
                        KMPoUp.ShowMessage(controller: self, message: "تم ارسال رقم المرور الجديد الي البريد الالكتروني", image: #imageLiteral(resourceName: "like (3)"))
                        self.perform(#selector(self.Cancel(_:)), with: nil, afterDelay: 4.0)
                    } else if result == "not found user" {
                        KMPoUp.ShowMessage(controller: self, message: "هذا البريد غير موجود يرجى إدخال بريد صحيح", image: #imageLiteral(resourceName: "warning"))
                    } else if result == "error" {
                        KMPoUp.ShowMessage(controller: self, message: "حدث خطأ غير متوقع يرجى المحاولة مرة أخرى", image: #imageLiteral(resourceName: "warning"))
                    }
                    self.indicator.stopAnimating()
                   
                case .failure(let err):
                    SharedHandller.MessageHandler(viewContro: self, title: "خطأ", message: err.localizedDescription, submitButton: "تم")
                }
        }
    }
}
