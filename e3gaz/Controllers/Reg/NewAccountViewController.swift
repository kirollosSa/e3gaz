//
//  NewAccountViewController.swift
//  e3gaz
//
//  Created by apple on 2/26/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import Alamofire
import KMPopUp
import SwiftyJSON

class NewAccountViewController: UIViewController {
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func CreateAccount(_ sender: Any) {
        if SharedHandller.isValidAction(textFeilds: [nameText,password]) {
            let whitespace = NSCharacterSet.whitespaces
            
            let range = nameText.text!.rangeOfCharacter(from: whitespace)
            if let test = range  {
                 KMPoUp.ShowMessageWithDuration(controller: self, message: "يرجى عدم استخدام مسافات في اسم المستخدم", image: #imageLiteral(resourceName: "warning"), duration: 2.0)
               
            } else {
                let param = ["method":"register",
                             "name":"\(nameText.text!)",
                    "phone":"\(phoneNumber.text!)",
                    "email":"\(emailText.text!)",
                    "password":"\(password.text!)"]
                doRequest(param: param)
            }
            
              
        } else {
            KMPoUp.ShowMessageWithDuration(controller: self, message: "يرخى ادخال البيانات المطلوبة", image: #imageLiteral(resourceName: "warning"), duration: 2.0)
        }
    }
    
    func doRequest(param: Parameters) {
        indicator.startAnimating()
        Alamofire.request(URLHandller.BASE_URL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
            .responseJSON { res in
                switch res.result {
                    
                case .success(let val):
                    let json = JSON(val)
                    
                    if let result = json["result"].string {
                        if result == "1" {
                            // read Data
                            
                            let id = json["id"].int
                            UserDefaults.standard.setValue(String(describing: id!), forKey: "DEFuserid")
                            
                            UserDefaults.standard.setValue(self.nameText.text!, forKey: "DEFusername")
                            
                            UserDefaults.standard.setValue(self.phoneNumber.text!, forKey: "DEFuserphone")
                            
                            UserDefaults.standard.setValue(self.emailText.text!, forKey: "DEFemail")
                            
                            UserDefaults.standard.setValue(APP_LOGIN, forKey: "login_State")
                            UserDefaults.standard.synchronize()
                            
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "GifVC") as! GifVC
                            
                            self.navigationController?.present(vc, animated: true, completion: nil)
                            
                        } else if result == "repate email" {
                            // fail dublication
                            KMPoUp.ShowMessageWithDuration(controller: self, message: "البريد الألكتروني مستخدم من قبل", image: #imageLiteral(resourceName: "warning"), duration: 1.5)
                            self.indicator.stopAnimating()
                        } else if result == "repate name" {
                            KMPoUp.ShowMessageWithDuration(controller: self, message: "اسم المستخدم موجود مسبقا", image: #imageLiteral(resourceName: "warning"), duration: 1.5)
                            
                            self.indicator.stopAnimating()
                        } else if result == "repate phone" {
                            // fail dublication
                             KMPoUp.ShowMessageWithDuration(controller: self, message: "رقم الهاتف مستخدم من قبل", image: #imageLiteral(resourceName: "warning"), duration: 1.5)
                            
                            self.indicator.stopAnimating()
                        }
                    }
                case .failure(let err):
                    KMPoUp.ShowMessage(controller: self, message: err.localizedDescription, image: #imageLiteral(resourceName: "warning"))
                }
        }
    }
}
