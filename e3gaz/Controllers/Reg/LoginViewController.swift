//
//  LoginViewController.swift
//  e3gaz
//
//  Created by apple on 2/26/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import KMPopUp

class LoginViewController: UIViewController {

    
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var phoneOrMail: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        if SharedHandller.Value(of: "inTestinPhase") == "false" {
            skipButton.isHidden = true
        } else {
            skipButton.isHidden = false
        }
        
        // Do any additional setup after loading the view.
    }

    @IBAction func skipButton(_ sender: Any) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "MainTabBar") as UIViewController
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    @IBAction func loginButton(_ sender: Any) {
        if SharedHandller.isValidAction(textFeilds: [phoneOrMail,password]) {
            let param = ["method":"login",
                         "name":"\(phoneOrMail.text!)",
                         "password":"\(password.text!)"
            ]
            AppLogin(param: param)
        } else {
            KMPoUp.ShowMessageWithDuration(controller: self, message: COMPLETE_DATA_ERROR, image: #imageLiteral(resourceName: "warning"), duration: 2.0)
        }
    }
    
    func AppLogin(param : Parameters) {
        indicator.startAnimating()
        Alamofire.request(URLHandller.BASE_URL,  method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
            .responseJSON { res in
                switch res.result {
                    
                case .success(let val):
                    let json = JSON(val)
                    if json.array == nil {
                            // fail dublication
                           
                            KMPoUp.ShowMessage(controller: self, message: "خطأ في البريد الالكتروني / رقم الهاتف أو كلمة المرور", image: #imageLiteral(resourceName: "warning"))
                            self.indicator.stopAnimating()
                       
                    } else {
                        // read Data
                        
                        var id : String = "null"
                        var name : String = "null"
                        var phone : String = "null"
                        var email : String = "null"
                        
                        if let Pid = json[0]["id"].string { id = Pid }
                        if let Pname = json[0]["name"].string { name = Pname }
                        if let Pphone = json[0]["phone"].string { phone = Pphone }
                        if let Pemail = json[0]["email"].string { email = Pemail }
                        
                        UserDefaults.standard.setValue(id, forKey: "DEFuserid")
                        UserDefaults.standard.setValue(name, forKey: "DEFusername")
                        
                        UserDefaults.standard.setValue(phone, forKey: "DEFuserphone")
                        UserDefaults.standard.setValue(email, forKey: "DEFemail")
                        UserDefaults.standard.setValue(APP_LOGIN, forKey: "login_State")
                        UserDefaults.standard.synchronize()
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "GifVC") as! GifVC
                        
                        self.navigationController?.present(vc, animated: true, completion: nil)
                    }
                case .failure(let err):
                    SharedHandller.MessageHandler(viewContro: self, title: "خطأ", message: err.localizedDescription, submitButton: "تم")
                }
        }
    }

}
