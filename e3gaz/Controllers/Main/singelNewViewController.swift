//
//  singelNewViewController.swift
//  e3gaz
//
//  Created by apple on 2/21/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import SDWebImage
import KMPopUp
import CoreData
import Alamofire
import SwiftyJSON

class singelNewViewController: UIViewController {
    
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var subDate: UILabel!
    @IBOutlet weak var subImage: UIImageView!
    @IBOutlet weak var subDetails: UILabel!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var followView: UIView!
    @IBOutlet weak var followStateImage: UIImageView!
    @IBOutlet weak var followStateText: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    var followState = false
    
    var isNotification = false
    var elementsDetails : News!
    let fetchRequest  : NSFetchRequest<E3gazEntityMo> = E3gazEntityMo.fetchRequest()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        if isNotification{
            getPost()
            saveButton.isHidden = true
        } else {
            sendView()
            if elementsDetails.subject_id != "0" {
                checkFollowState()
            }
            subTitle.text = elementsDetails.name!
            subDate.text = SharedHandller.changeDate(dates: elementsDetails.created!)
            subDetails.text = elementsDetails.details!
            setImage(url: "\(URLHandller.headerUrl(base_url: URLHandller.APPURL.posts))\(elementsDetails.image!)")
        }
      
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func saveButton(_ sender: Any) {
        
        
        let predicate = NSPredicate(format: "id == \(elementsDetails.id!)")
        
        fetchRequest.predicate = predicate
        do
        {
            
            let test = try PresistanceServce.contecxt.fetch(fetchRequest)
            
            if test.count > 0
            {
                KMPoUp.ShowMessageWithDuration(controller: self, message: "هذا المحتوى محفوظ مسبقا", image: #imageLiteral(resourceName: "warning"), duration: 2.0)
            } else {
                let subject = E3gazEntityMo(context: PresistanceServce.contecxt)
                
                
                subject.id  = elementsDetails.id!
                subject.name  = elementsDetails.name!
                subject.image  = elementsDetails.image!
                subject.details  = elementsDetails.details!
                subject.created  = elementsDetails.id!
                subject.subject_id = elementsDetails.subject_id!
                
                PresistanceServce.saveContext()
                KMPoUp.ShowMessageWithDuration(controller: self, message: "تم حفظ المحتوى في اعجازي", image: #imageLiteral(resourceName: "like (3)"), duration: 2.0)
            }
        }
        catch
        {
            print(error)
        }
        
    }
    
    @IBAction func followButton(_ sender: Any) {
        if followState {
            DoFollowAction(for: "delete_subject")
        } else {
            DoFollowAction(for: "add_subject")
        }
    }
    
    
    func setImage(url : String) {
        DispatchQueue.main.async {
            self.subImage.sd_setImage(with: URL(string: url))
        }
    }
    
    func sendView() {
        let param = ["method":"update_post",
                     "post_id":"\(elementsDetails.id!)"]
        Alamofire.request(URLHandller.BASE_URL,  method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
            .responseJSON { res in
                switch res.result {
                    
                case .success(let val):
                    let json = JSON(val)
                case .failure( let err):
                    print(err.localizedDescription)
                }
        }
    }
    
    func checkFollowState() {
        
        let param = ["method":"check_subject",
                     "device_id":"\(DEVICE_ID)",
                     "subject_id":"\(elementsDetails.subject_id!)"]
        Alamofire.request(URLHandller.BASE_URL,  method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
            .responseJSON { res in
                switch res.result {
                    
                case .success(let val):
                    let json = JSON(val)
                    let result = json["result"].string!
                    if result == "0" {
                        self.followView.backgroundColor = UIColor(hexString: "#6C6969")
                        self.followStateImage.image = UIImage(named: "48")
                        self.followStateText.text = "الموضوع متابع"
                        self.followState = true
                        self.followView.isHidden = false
                    } else if result == "1" {
                        self.followView.backgroundColor = UIColor(hexString: "#5192F5")
                        self.followStateImage.image = UIImage(named: "47")
                        self.followStateText.text = "متابعة الموضوع"
                        self.followState = false
                        self.followView.isHidden = false
                    }
                case .failure( let err):
                    print(err.localizedDescription)
                }
        }
    }
    
    func DoFollowAction(for method: String) {
        let param = ["method":"\(method)",
                     "device_id":"\(DEVICE_ID)",
                     "subject_id":"\(elementsDetails.subject_id!)"]
        
        Alamofire.request(URLHandller.BASE_URL,  method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
            .responseJSON { res in
                switch res.result {
                    
                case .success(let val):
                    let json = JSON(val)
                    let result = json["result"].string!
                    if result == "1" {
                        if method == "add_subject" {
                            self.followState = true
                            self.followView.backgroundColor = UIColor(hexString: "#6C6969")
                            self.followStateImage.image = UIImage(named: "48")
                            self.followStateText.text = "الموضوع متابع"
                        } else if method == "delete_subject"  {
                            self.followState = false
                            self.followStateImage.image = UIImage(named: "47")
                            self.followView.backgroundColor = UIColor(hexString: "#5192F5")
                            self.followStateText.text = "متابعة الموضوع"
                        }
                    } else if result == "0" {
                        KMPoUp.ShowMessage(controller: self, message: "حدث خطأ غير متوقع يرجى المحاولة مرة اخرى", image: #imageLiteral(resourceName: "warning"))
                    }
                case .failure( let err):
                    print(err.localizedDescription)
                }
        }
    }
    
    func getPost() {
        let param = ["method":"get_post_id",
                     "post_id":"\(SharedHandller.Value(of: "TARGET_ID"))"]
        Alamofire.request(URLHandller.BASE_URL,  method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
            .responseJSON { res in
                switch res.result {
                    
                case .success:
                    if let Data = res.result.value as? NSArray{
                        for data in Data{
                            
                            if let object = data as? Dictionary <String, Any>{
                                if let name = object["name"] as? String {
                                    self.navigationItem.title = name
                                }
                                if let det = object["details"] as? String{
                                    self.subDetails.text = det
                                }
                                if let image = object["image"] as? String{
                                    self.setImage(url: "\(URLHandller.headerUrl(base_url: URLHandller.APPURL.posts))\(image)")
                                }
                            }
                        }
                    }
                case .failure( let err):
                    print(err.localizedDescription)
                }
        }
    }
    
    
}
