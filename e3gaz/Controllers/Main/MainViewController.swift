//
//  MainViewController.swift
//  e3gaz
//
//  Created by apple on 2/21/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var lastArticle: UIView!
    @IBOutlet weak var segmentView: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()

       
        
        segmentView.selectedSegmentIndex = 1
        // Do any additional setup after loading the view.
    }

    
    @IBAction func searchButton(_ sender: Any) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func sideButton(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SidePageViewController") as! SidePageViewController
        
        navigationController?.pushViewController(vc, animated: true)
    }
   
    @IBAction func segmentView(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 1 {
            UIView.animate(withDuration: 0.5, animations: {
              //  self.newImages.alpha = 0.0
                self.lastArticle.alpha = 1.0
            })
        } else if sender.selectedSegmentIndex == 0 {
            UIView.animate(withDuration: 0.5, animations: {
                //  self.newImages.alpha = 1.0
                self.lastArticle.alpha = 0.0
            })
        }
    }
}
