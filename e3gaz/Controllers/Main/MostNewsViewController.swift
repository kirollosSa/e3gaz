//
//  MostNewsViewController.swift
//  e3gaz
//
//  Created by apple on 2/21/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import KMPopUp

class MostNewsViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyBox: UIImageView!
    
    var List = [News]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadNews()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return List.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : SubjectsTableViewCell!
        
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! SubjectsTableViewCell
            cell.setImage(url:"\( URLHandller.headerUrl(base_url: URLHandller.APPURL.posts))\(List[indexPath.row].image!)")
            cell.subDate.text = SharedHandller.changeDate(dates: List[indexPath.row].created!)
            cell.subTitle.text = List[indexPath.row].name!
            cell.selectionStyle = .none
            return cell
        }
        else {
            cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! SubjectsTableViewCell
            cell.setImage(url:"\( URLHandller.headerUrl(base_url: URLHandller.APPURL.posts ))\(List[indexPath.row].image!)")
            cell.subDate.text = SharedHandller.changeDate(dates: List[indexPath.row].created!)
            cell.subTitle.text = List[indexPath.row].name!
            cell.subDetails.text = List[indexPath.row].details!
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "singelNewViewController") as! singelNewViewController
        vc.elementsDetails = List[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
        
    }

    func loadNews() {
        let param = ["method":"news"]
        self.indicator.startAnimating()
        Alamofire.request(URLHandller.BASE_URL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil)
            .responseJSON { res in
                switch res.result {
                    
                case .success(let val):
                    let json = JSON(val)
                    let new = json["more_scenes"].array!
                    if new.count > 0 {
                        for index in 0..<new.count {
                            
                            var id : String = "nil"
                            var name: String = "nil"
                            var image: String = "nil"
                            var subject_id : String = "nill"
                            var details: String = "nil"
                            var youtube: String = "nil"
                            var created: String = "nil"
                            
                            if let pid = new[index]["id"].string { id = pid }
                            if let pname = new[index]["name"].string { name = pname }
                            if let pimage = new[index]["image"].string { image = pimage }
                            if let pdetails = new[index]["details"].string { details = pdetails }
                            if let psubject_id = new[index]["subject_id"].string { subject_id = psubject_id }
                            if let pyoutube = new[index]["youtube"].string { youtube = pyoutube }
                            if let pcreated = new[index]["created"].string { created = pcreated }
                            
                                self.List.append(News(id: id, name: name, details: details, image: image, created: created, subject_id: subject_id, youtube: youtube))
                           
                        }
                        
                        self.indicator.stopAnimating()
                        self.tableView.isHidden = false
                        self.tableView.reloadData()
                    } else {
                        self.emptyBox.isHidden = false
                    }
                    
                case .failure(let err):
                    self.indicator.stopAnimating()
                    KMPoUp.ShowMessage(controller: self, message: err.localizedDescription, image: #imageLiteral(resourceName: "warning"))
                }
        }
    }

}
