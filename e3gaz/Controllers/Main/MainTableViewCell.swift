//
//  MainTableViewCell.swift
//  e3gaz
//
//  Created by apple on 2/21/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class MainTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }



}

class NewsVideoCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var webView: UIWebView!
    
    func setVideo(videoCode: String) {
        webView.loadRequest(URLRequest(url: URL(string: "\(URLHandller.youtubeBaseUrl)\(videoCode)")!))
    }
    
}
