//
//  AppDelegate.swift
//  e3gaz
//
//  Created by apple on 2/13/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import FirebaseAuth
import Firebase
import SwiftyJSON
import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging
import Alamofire

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
       
        IQKeyboardManager.shared.enable = true
        UITabBarItem.appearance()
            .setTitleTextAttributes(
                [NSAttributedStringKey.font: UIFont(name: "HacenTunisia", size: 10)!],
                for: .normal)
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        FirebaseApp.configure()
        
        Messaging.messaging().delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshToken(notification:)), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        
        return true
    }
    
    func fbHandler(){
        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    @objc func refreshToken(notification: NSNotification){
        fbHandler()
    }
    func applicationWillTerminate(_ application: UIApplication) {
        PresistanceServce.saveContext()
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        fbHandler()
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        let data = JSON(userInfo)
        print("data didReceiveRemoteNotification: \(data)")
        if let notificationID = data["gcm.notification.notification_type"].string {
            if let target = data["gcm.notification.target_id"].string {
               if notificationID == "1"{
                    SharedHandller.setValue(target, forKey: "TARGET_ID")
                    NotificationCenter.default.post(name: TO_IMAGE, object: nil)
                }else if notificationID == "3"{
                    SharedHandller.setValue(target, forKey: "TARGET_ID")
                    NotificationCenter.default.post(name: TO_VIDEO, object: nil)
                }else if notificationID == "4"{
                    SharedHandller.setValue(target, forKey: "TARGET_ID")
                    NotificationCenter.default.post(name: TO_BOOK, object: nil)
                }else if notificationID == "5"{
                    SharedHandller.setValue(target, forKey: "TARGET_ID")
                    NotificationCenter.default.post(name: TO_ARTICLE, object: nil)
                }
                
            }
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        
        let data = JSON(userInfo)
        print("data didReceiveRemoteNotification: \(data)")
        if let notificationID = data["gcm.notification.notification_type"].string {
            if let target = data["gcm.notification.target_id"].string {
                if notificationID == "1"{
                    SharedHandller.setValue(target, forKey: "TARGET_ID")
                    NotificationCenter.default.post(name: TO_IMAGE, object: nil)
                    
                }else if notificationID == "3"{
                    SharedHandller.setValue(target, forKey: "TARGET_ID")
                    NotificationCenter.default.post(name: TO_VIDEO, object: nil)
                }else if notificationID == "4"{
                    SharedHandller.setValue(target, forKey: "TARGET_ID")
                    NotificationCenter.default.post(name: TO_BOOK, object: nil)
                }else if notificationID == "5"{
                    SharedHandller.setValue(target, forKey: "TARGET_ID")
                    NotificationCenter.default.post(name: TO_ARTICLE, object: nil)
                }
                
            }
        }
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("APNs registration failed: \(error)")
    }
    
    
    
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        print("APNs device token: \(deviceTokenString)")
        
        if let refreshedToken = InstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            UserDefaults.standard.setValue(refreshedToken, forKey: DEVICE_TOKEN)
            sendToken()
            UserDefaults.standard.synchronize()
        }
    }
}


//MARK: forgorund
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo as! [String : Any]
        print(userInfo)
        completionHandler([.alert, .badge, .sound])
    }
    
    //MARK: ON Click Local Notification Forground
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        print(userInfo)
        
        let data = JSON(userInfo)
        print("data didReceive response: \(data)")
        
        if let notificationID = data["gcm.notification.notification_type"].string {
            if let target = data["gcm.notification.target_id"].string {
                let state = UIApplication.shared.applicationState
                if state == .background || state == .active {
                    print("App in Background")
                    if notificationID == "0"{
                        completionHandler()
                    }
                    else if notificationID == "1"{
                        let myTabBar = self.window?.rootViewController as! UITabBarController // Getting Tab Bar
                        myTabBar.selectedIndex = 2
                        SharedHandller.setValue(target, forKey: "TARGET_ID")
                        NotificationCenter.default.post(name: TO_IMAGE, object: nil)
                    }else if notificationID == "3"{
                        let myTabBar = self.window?.rootViewController as! UITabBarController // Getting Tab Bar
                        myTabBar.selectedIndex = 1
                        SharedHandller.setValue(target, forKey: "TARGET_ID")
                        perform(#selector(RunVideoObserver), with: nil, afterDelay: 0.5)
                    }else if notificationID == "4"{
                        let myTabBar = self.window?.rootViewController as! UITabBarController // Getting Tab Bar
                        myTabBar.selectedIndex = 2
                        SharedHandller.setValue(target, forKey: "TARGET_ID")
                        NotificationCenter.default.post(name: TO_BOOK, object: nil)
                    }else if notificationID == "5"{
                        let myTabBar = self.window?.rootViewController as! UITabBarController // Getting Tab Bar
                        myTabBar.selectedIndex = 2
                        SharedHandller.setValue(target, forKey: "TARGET_ID")
                        NotificationCenter.default.post(name: TO_ARTICLE, object: nil)
                    }
                } else {
                    if notificationID == "0"{
                        completionHandler()
                    }
                    else if notificationID == "1"{
                        SharedHandller.setValue(target, forKey: "TARGET_ID")
                        perform(#selector(toImage), with: nil, afterDelay: 3.5)
                    } else if notificationID == "3"{
                        
                        SharedHandller.setValue(target, forKey: "TARGET_ID")
                        perform(#selector(toVideo), with: nil, afterDelay: 3.5)
                    }else if notificationID == "4"{
                        SharedHandller.setValue(target, forKey: "TARGET_ID")
                        perform(#selector(toBook), with: nil, afterDelay: 3.5)
                    }else if notificationID == "5"{
                        SharedHandller.setValue(target, forKey: "TARGET_ID")
                        perform(#selector(toArticle), with: nil, afterDelay: 3.5)
                    }
                }

            }
        }
        completionHandler()
    }
    
    //MARK: Helper Methods
    
    @objc func toImage() {
        print("InActive")
        let myTabBar = self.window?.rootViewController as! UITabBarController // Getting Tab Bar
        myTabBar.selectedIndex = 2
        NotificationCenter.default.post(name: TO_IMAGE, object: nil)
    }
    @objc func toVideo() {
        print("InActive")
        let myTabBar = self.window?.rootViewController as! UITabBarController // Getting Tab Bar
        myTabBar.selectedIndex = 1
        perform(#selector(RunVideoObserver), with: nil, afterDelay: 0.5)
    }
    @objc func RunVideoObserver() {
        NotificationCenter.default.post(name: TO_VIDEO, object: nil)
    }
    
    @objc func toBook() {
        print("InActive")
        let myTabBar = self.window?.rootViewController as! UITabBarController // Getting Tab Bar
        myTabBar.selectedIndex = 2
        NotificationCenter.default.post(name: TO_BOOK, object: nil)
    }
    @objc func toArticle() {
        print("InActive")
        let myTabBar = self.window?.rootViewController as! UITabBarController // Getting Tab Bar
        myTabBar.selectedIndex = 2
        NotificationCenter.default.post(name: TO_ARTICLE, object: nil)
    }
}

//MARK:backgorund
extension AppDelegate : MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        UserDefaults.standard.setValue(fcmToken, forKey: DEVICE_TOKEN)
        UserDefaults.standard.synchronize()
        sendToken()
        
    }
    
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Noti : \(remoteMessage.appData as? Dictionary<String, Any>)")
    }
    
    func sendToken() {
        let param = [
            "method"      : "setting_ios",
            "device_token": "\(SharedHandller.getDeviceToken())",
            "device_id"   : "\(DEVICE_ID)",
            "id"          : "\(SharedHandller.GetUserID())"]
        Alamofire.request(URLHandller.BASE_URL, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseJSON { res in
            switch res.result {
            case .success(_):
                if let value = res.result.value as? NSDictionary {
                    if let id = value["id"] as? String{
                        UserDefaults.standard.setValue(id, forKey: "DEFuserid")
                    }
                }
            case .failure:break
            }
        }
    }
}


