//
//  E3gazEntityMo+CoreDataProperties.swift
//  
//
//  Created by apple on 2/27/18.
//
//

import Foundation
import CoreData


extension E3gazEntityMo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<E3gazEntityMo> {
        return NSFetchRequest<E3gazEntityMo>(entityName: "E3gazEntity")
    }

    @NSManaged public var id: String?
    @NSManaged public var name: String?
    @NSManaged public var details: String?
    @NSManaged public var image: String?
    @NSManaged public var created: String?
    @NSManaged public var subject_id: String?

}
